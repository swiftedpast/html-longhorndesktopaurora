# HTML-LonghornDesktopAurora

A half-arsed attempt at recreating the Desktop Aurora featured in Windows Codenamed "Longhorn" 405x build using SVGs and CSS animations.

## Notice

Attempted as a port of XAML to SVG, the co-ordinate systems and the way transforms, scaling and rotation work in both systems; the values cannot be used directly and would need "parsing".

There are some inaccuracies such as the gradients and locations of some of the elements particularly the thin light rays.